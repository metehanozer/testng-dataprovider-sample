package mt.util;

import com.google.gson.Gson;
import com.google.gson.internal.Primitives;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class JsonManager {

    private static final String resources = "/src/test/resources/data/";

    public String getFilePathFromName(String fileName) {
        return System.getProperty("user.dir") + resources + fileName;
    }

    public <T> T jsonToModel(String filename, Class<T> classOfT) {
        String filePath = getFilePathFromName(filename);
        Object object = null;

        try {
            Reader reader = Files.newBufferedReader(Paths.get(filePath), StandardCharsets.UTF_8);
            object = new Gson().fromJson(reader, classOfT);
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        }

        return Primitives.wrap(classOfT).cast(object);
    }
}
