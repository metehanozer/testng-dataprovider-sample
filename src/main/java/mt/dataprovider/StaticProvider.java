package mt.dataprovider;

import org.testng.annotations.DataProvider;

public class StaticProvider {

    /**
     * STATIC DATA PROVIDER THAT RETURN 2D ARRAY
     */
    @DataProvider
    public static Object[][] staticDataProvider() {
        return new Object[][]{
                {"Test Data 1"},
                {"Test Data 2"},
                {"Test Data 3"}
        };
    }
}
