package mt.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Sample {

    public int value1;
    public int value2;
    public int expectedResult;
    public String errorMessage;
}
