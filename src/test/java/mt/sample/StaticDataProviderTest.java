package mt.sample;

import mt.dataprovider.StaticProvider;
import org.testng.annotations.Test;

public class StaticDataProviderTest {

    @Test(dataProvider = "staticDataProvider", dataProviderClass = StaticProvider.class)
    void array2dDataProviderSample(String testData) {
        System.out.println("Static data provider - " + testData);
    }
}
