package mt.sample;

import mt.model.User;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class ModelIteratorDataProviderTest {

    /**
     * DATA PROVIDER THAT RETURN MODEL
     */
    @DataProvider
    public Object[][] modelDataProvider() {
        List<User> userList = new ArrayList<>();

        userList.add(new User("User 1", 21));
        userList.add(new User("User 2", 22));
        userList.add(new User("User 3", 23));

        int i = 0;
        Object[][] objects = new Object[userList.size()][1];
        for (Object[] object : objects)
            object[0] = userList.get(i++);

        return objects;
    }

    @Test(dataProvider = "modelDataProvider")
    void modelDataProviderSample(User user) {
        System.out.println("Model data provider - " + user);
    }
}
