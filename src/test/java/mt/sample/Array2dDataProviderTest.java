package mt.sample;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Array2dDataProviderTest {

    /**
     * DATA PROVIDER THAT RETURN 2D ARRAY
     */
    @DataProvider
    public Object[][] array2dDataProvider() {
        return new Object[][]{
                {"Test Data 1"},
                {"Test Data 2"},
                {"Test Data 3"}
        };
    }

    @Test(dataProvider = "array2dDataProvider")
    void array2dDataProviderSample(String testData) {
        System.out.println("2d array data provider - " + testData);
    }
}
