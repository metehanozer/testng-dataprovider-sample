package mt.sample;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ArrayIteratorDataProviderTest {

    /**
     * DATA PROVIDER THAT RETURN ARRAY ITERATOR
     */
    @DataProvider
    public Iterator<Object[]> arrayIteratorDataProvider() {
        List<Object[]> dataList = new ArrayList<>();

        dataList.add(new Object[]{"Test Data 1"});
        dataList.add(new Object[]{"Test Data 2"});
        dataList.add(new Object[]{"Test Data 3"});

        return dataList.iterator();
    }

    @Test(dataProvider = "arrayIteratorDataProvider")
    void arrayIteratorDataProviderSample(String testData) {
        System.out.println("Array iterator data provider - " + testData);
    }
}
