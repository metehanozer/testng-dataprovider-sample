package mt.sample;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MultiValuesDataProviderTest {

    /**
     * DATA PROVIDER THAT RETURN MULTI VALUES
     */
    @DataProvider
    public Object[][] multiValuesDataProvider() {
        return new Object[][]{
                {"Data 1", 11},
                {"Data 2", 12},
                {"Data 3", 13}
        };
    }

    @Test(dataProvider = "multiValuesDataProvider")
    void multiValuesDataProviderSample(String data1, int data2) {
        System.out.println("Multi values data provider - " + data1 + " - " + data2);
    }
}
