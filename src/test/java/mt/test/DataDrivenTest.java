package mt.test;

import mt.model.Sample;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class DataDrivenTest {

    @DataProvider
    public Object[][] dataProvider() {
        List<Sample> sampleList = new ArrayList<>();

        sampleList.add(new Sample(2, 3, 5, "2 plus 3 must be 5!"));
        sampleList.add(new Sample(9, 2, 10, "9 plus 2 must be 11!"));
        sampleList.add(new Sample(100, 1, 101, "100 plus 1 must be 101!"));

        int i = 0;
        Object[][] objects = new Object[sampleList.size()][1];
        for (Object[] object : objects)
            object[0] = sampleList.get(i++);

        return objects;
    }

    @Test(dataProvider = "dataProvider")
    void dataDrivenTest(Sample sample) {
        int result = sample.value1 + sample.value2;
        Assert.assertEquals(result, sample.expectedResult, sample.errorMessage);
    }
}
