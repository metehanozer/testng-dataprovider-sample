package mt.test;

import mt.model.Sample;
import mt.util.JsonManager;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class JsonDataDrivenTest {

    @DataProvider
    public Object[][] dataProvider() {
        Sample[] samples = new JsonManager().jsonToModel("sample.json", Sample[].class);

        int i = 0;
        Object[][] objects = new Object[samples.length][1];
        for (Object[] object : objects)
            object[0] = samples[i++];

        return objects;
    }

    @Test(dataProvider = "dataProvider")
    void jsonDataProviderSample(Sample sample) {
        int result = sample.value1 + sample.value2;
        Assert.assertEquals(result, sample.expectedResult, sample.errorMessage);
    }
}
